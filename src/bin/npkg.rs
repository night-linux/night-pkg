fn main() {
    let package = night_pkg::Package::from_manifest(toml::toml! {
        [package]
        name = "test_package"
        version = "1.0.0"
        [dependencies]
        another_pkg = "0.1.0"
    });
    println!("{:#?}", package);
}
