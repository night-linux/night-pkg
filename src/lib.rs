pub mod package_table;
#[derive(Debug)]
pub struct Dependency {
    name: String,
    version: semver::VersionReq,
}
#[derive(Debug)]
pub struct Package {
    name: String,
    version: semver::Version,
    dependencies: Vec<Dependency>,
}
impl Package {
    pub fn from_manifest(manifest: toml::Value) -> Self {
        let dependencies_toml = manifest["dependencies"].as_table().unwrap();

        Self {
            name: manifest["package"]["name"].as_str().unwrap().to_string(),
            version: semver::Version::parse(manifest["package"]["version"].as_str().unwrap()).unwrap(),
            dependencies: dependencies_toml
                .iter()
                .map(|entry| Dependency {
                    name: entry.0.clone(),
                    version: semver::VersionReq::parse(entry.1.as_str().unwrap()).unwrap(),
                })
                .collect(),
        }
    }
    pub fn new(name: &str, version: &str, dependencies: Vec<Dependency>) -> Self {
        Self {
            name: name.to_string(),
            version: semver::Version::parse(version).unwrap(),
            dependencies,
        }
    }
}
